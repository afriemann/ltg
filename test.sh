#!/bin/bash
# runme.sh
# created: 06.06.2015
# author: Aljosha Friemann <aljosha.friemann@gmail.com>

# directory safety. does not resolve links!
# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

log () {
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~ $@ ~~~~~~~~~~~~~~~~~~~~~~~~~~"
}

TEST_ROOT="/tmp/ltg-test"
TEST_SRC="$TEST_ROOT/src"
TEST_DST="$TEST_ROOT/dst"

LTG_OPTS="--storage-dir $TEST_SRC --link-dir $TEST_DST --debug"

ltg () {
    set -x
    python3 -m ltg $LTG_OPTS "$@"
    set +x
}

lsr () {
    log "root content:"
    echo
    ls --color=auto -lAR -I ".git" "$TEST_ROOT"
    echo
}

setup_test_folder () {
    mkdir -p "$TEST_ROOT/.config" "$TEST_DST" || :

    touch "$TEST_ROOT/test1.conf" "$TEST_ROOT/.config/test2.conf" || :

    mkdir -p "$TEST_ROOT/.vim/bundle/Vundle.vim/.git"  || :
    touch  "$TEST_ROOT/.vim/vimrc" "$TEST_ROOT/.vim/bundle/Vundle.vim/README.md" || :
}

remove_test_folder () {
    [ -d "$TEST_ROOT" ] && rm -rf "$TEST_ROOT"
}

remove_test_folder

##############################

setup_test_folder

ltg info

lsr

log "adding files"
ltg store --relative-to "$TEST_ROOT" "$TEST_ROOT/.config"
ltg store --relative-to "$TEST_ROOT" "$TEST_ROOT/.vim" "$TEST_ROOT/test1.conf"

log "linking files"
ltg link

lsr

log "moving files"
ltg mv "$TEST_DST/test1.conf" "$TEST_DST/.config/test1.conf"
ltg mv "$TEST_DST"/.config/* "$TEST_DST"/config/
ltg mv "$TEST_DST/.vim" "$TEST_DST/vim"

lsr

log "git stuff"
echo "abc" >> "$TEST_DST"/.vim/vimrc

ltg git -- status
ltg git -- add -A
# ltg git -- commit --interactive
ltg git -- commit -m "changed vimrc"

log "unlinking files"
ltg unlink

lsr

remove_test_folder

##############################

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 fenc=utf-8
